
function calculate() {
    a = parseFloat(document.calculator.a.value);
    b = parseFloat(document.calculator.b.value);
    c = parseFloat(document.calculator.c.value);

 if (a == 0) {

   ans1 = "NA";
   ans2 = "NA";

 } else {

   disc = b*b - 4*a*c;

   if (disc < 0) {

     real = -b / (2 * a);
     imag = Math.sqrt(-disc) / (2 * a);

     ans1 = real + " + " + Math.abs(imag) + "i";
     ans2 = real + " - " + Math.abs(imag) + "i";


   } else if (disc == 0) {

     ans1 = -b / (2 * a);
     ans2 = "NA";

   } else {

     note = "";
     ans1 = (-b + Math.sqrt(disc))/(2*a);
     ans2 = (-b - Math.sqrt(disc))/(2*a);

   }
 }

 formula = "";
 formula += NeatAdd(formula, a, "x²");
 formula += NeatAdd(formula, b, "x");
 formula += NeatAdd(formula, c, "");

    document.calculator.equn.value = formula + " = 0";
    document.calculator.disc.value = disc;
    document.calculator.ans1.value = ans1;
    document.calculator.ans2.value = ans2;
    document.calculator.note.value = note;

}
function NeatAdd(formula, x, varname) {

 s = "";

 if (formula.length==0) {
   plus = "";
 } else {
   plus = " + ";
 }
 minus = " - ";

 if (x==1) {
   if (varname.length==0) {
     s = s + plus + "1";
   } else {
     s = s + plus + varname;
   }
 } else if (x == -1) {
   if (varname.length==0) {
     s = s + minus + "1";
   } else {
     s = s + minus + varname;
   }
 } else if (x == 0) {
 } else if (x > 0) {
     s = s + plus + x + varname;
 } else {
     s = s + minus + Math.abs(x) + varname;
 }

 return s;
}
